import sys

from numpy import zeros, float32
#  pgmpy
import pgmpy
import random
from pgmpy.models import BayesianModel
from pgmpy.factors.discrete import TabularCPD
from pgmpy.inference import VariableElimination


# You are not allowed to use following set of modules from 'pgmpy' Library.
#
# pgmpy.sampling.*
# pgmpy.factors.*
# pgmpy.estimators.*

def make_power_plant_net():
    """Create a Bayes Net representation of the above power plant problem. 
    Use the following as the name attribute: "alarm","faulty alarm", "gauge","faulty gauge", "temperature". (for the tests to work.)
    """

    BayesNet = BayesianModel()
    BayesNet.add_node("alarm")
    BayesNet.add_node("temperature")
    BayesNet.add_node("faulty alarm")
    BayesNet.add_node("gauge")
    BayesNet.add_node("faulty gauge")

    BayesNet.add_edge("faulty alarm", "alarm")
    BayesNet.add_edge("gauge", "alarm")
    BayesNet.add_edge("faulty gauge", "gauge")
    BayesNet.add_edge("temperature", "gauge")
    BayesNet.add_edge("temperature", "faulty gauge")

    return BayesNet


def set_probability(bayes_net):
    """Set probability distribution for each node in the power plant system.
    Use the following as the name attribute: "alarm","faulty alarm", "gauge","faulty gauge", "temperature". (for the tests to work.)
    """

    # bayes_net = make_power_plant_net()

    # P(T) = 0.2 P(~T)=0.8
    # P(FA) = 0.15 P(~FA)=0.85
    # P(FG|T) = 0.8 P(FG|~T) = 0.05
    # P(A|FA,G) = 0.55 P(A|~FA,G) = 0.9
    # P(A|FA,~G)=0.45 P(A)=(~FA,~G) 0.1
    # P(G|FG,T) = 0.2  P(G|~FG,T) = 0.95
    # P(G|FG,~T) = 0.8 P(G|~FG,~T) = 0.05

    # The temperature is hot (call this "true") 20% of the time.
    # "temperature" (high = True, normal = False)
    t = TabularCPD('temperature', 2, values=[[0.80], [0.20]])

    # The alarm is faulty 15% of the time.
    fa = TabularCPD('faulty alarm', 2, values=[[0.85], [0.15]])

    # "gauge" (high = True, normal = False)

    # The temperature gauge reads the correct temperature with 95% probability when it is not faulty and 20% probability
    # when it is faulty. For simplicity, say that the gauge's "true" value corresponds with its "hot" reading and "false"
    # with its "normal" reading, so the gauge would have a 95% chance of returning "true" when the temperature is hot and it is not faulty.
    fg_t = TabularCPD('faulty gauge', 2, values=[[0.95, 0.2], [0.05, 0.8]], evidence=['temperature'], evidence_card=[2])

    # When the temperature is hot, the gauge is faulty 80% of the time. Otherwise, the gauge is faulty 5% of the time.
    # t_fg = TabularCPD('A', 2, values=[[0.95, 0.05], [0.2, 0.8]], evidence=['T'], evidence_card=[2])
    g_fg_t = TabularCPD('gauge', 2, values=[[0.95, 0.05, 0.2, 0.8], [0.05, 0.95, 0.8, 0.2]],
                        evidence=['faulty gauge', 'temperature'], evidence_card=[2, 2])

    # The alarm responds correctly to the gauge 55% of the time when the alarm is faulty, and it responds correctly to the gauge 90% of
    # the time when the alarm is not faulty. For instance, when it is faulty, the alarm sounds 55% of the time that the gauge is "hot"
    # and remains silent 55% of the time that the gauge is "normal."
    a_fa_g = TabularCPD('alarm', 2, values=[[0.9, 0.1, 0.55, 0.45], [0.1, 0.9, 0.45, 0.55]],
                        evidence=['faulty alarm', 'gauge'], evidence_card=[2, 2])

    bayes_net.add_cpds(t, fa, fg_t, g_fg_t, a_fa_g)

    return bayes_net


def get_alarm_prob(bayes_net):
    """Calculate the marginal probability of the alarm ringing in the power plant system."""

    solver = VariableElimination(bayes_net)
    marginal_prob = solver.query(variables=['alarm'], joint=False)
    alarm_prob = marginal_prob['alarm'].values

    return alarm_prob[1]


def get_gauge_prob(bayes_net):
    """Calculate the marginal probability of the gauge showing hot in the power plant system."""

    solver = VariableElimination(bayes_net)
    marginal_prob = solver.query(variables=['gauge'], joint=False)
    gauge_prob = marginal_prob['gauge'].values

    return gauge_prob[1]


def get_temperature_prob(bayes_net):
    """Calculate the conditional probability of the temperature being hot in the power
       plant system, given that the alarm sounds and neither the gauge nor alarm is faulty."""

    solver = VariableElimination(bayes_net)
    conditional_prob = solver.query(variables=['temperature'],
                                    evidence={'alarm': 1, 'faulty gauge': 0, 'faulty alarm': 0}, joint=False)
    temp_prob = conditional_prob['temperature'].values

    return temp_prob[1]


def get_game_network():
    """Create a Bayes Net representation of the game problem. Name the nodes as "A","B","C","AvB","BvC" and "CvA"."""

    BayesNet = BayesianModel()
    BayesNet.add_node('A')
    BayesNet.add_node('B')
    BayesNet.add_node('C')
    BayesNet.add_node('AvB')
    BayesNet.add_node('BvC')
    BayesNet.add_node('CvA')

    BayesNet.add_edge('A', 'AvB')
    BayesNet.add_edge('B', 'AvB')

    BayesNet.add_edge('B', 'BvC')
    BayesNet.add_edge('C', 'BvC')

    BayesNet.add_edge('C', 'CvA')
    BayesNet.add_edge('A', 'CvA')

    a_cpd = TabularCPD('A', 4, values=[[0.15], [0.45], [0.30], [0.10]])
    b_cpd = TabularCPD('B', 4, values=[[0.15], [0.45], [0.30], [0.10]])
    c_cpd = TabularCPD('C', 4, values=[[0.15], [0.45], [0.30], [0.10]])

    avb_cpd = TabularCPD('AvB', 3, values=[
        [0.1, 0.2, 0.15, 0.05, 0.6, 0.1, 0.2, 0.15, 0.75, 0.6, 0.1, 0.2, 0.9, 0.75, 0.6, 0.1],
        [0.1, 0.6, 0.75, 0.9, 0.2, 0.1, 0.6, 0.75, 0.15, 0.2, 0.1, 0.6, 0.05, 0.15, 0.2, 0.1],
        [0.8, 0.2, 0.1, 0.05, 0.2, 0.8, 0.2, 0.1, 0.1, 0.2, 0.8, 0.2, 0.05, 0.10, 0.2, 0.8],
    ], evidence=['A', 'B'], evidence_card=[4, 4])

    bvc_cpd = TabularCPD('BvC', 3, values=[
        [0.1, 0.2, 0.15, 0.05, 0.6, 0.1, 0.2, 0.15, 0.75, 0.6, 0.1, 0.2, 0.9, 0.75, 0.6, 0.1],
        [0.1, 0.6, 0.75, 0.9, 0.2, 0.1, 0.6, 0.75, 0.15, 0.2, 0.1, 0.6, 0.05, 0.15, 0.2, 0.1],
        [0.8, 0.2, 0.1, 0.05, 0.2, 0.8, 0.2, 0.1, 0.1, 0.2, 0.8, 0.2, 0.05, 0.10, 0.2, 0.8],
    ], evidence=['B', 'C'], evidence_card=[4, 4])

    cva_cpd = TabularCPD('CvA', 3, values=[
        [0.1, 0.2, 0.15, 0.05, 0.6, 0.1, 0.2, 0.15, 0.75, 0.6, 0.1, 0.2, 0.9, 0.75, 0.6, 0.1],
        [0.1, 0.6, 0.75, 0.9, 0.2, 0.1, 0.6, 0.75, 0.15, 0.2, 0.1, 0.6, 0.05, 0.15, 0.2, 0.1],
        [0.8, 0.2, 0.1, 0.05, 0.2, 0.8, 0.2, 0.1, 0.1, 0.2, 0.8, 0.2, 0.05, 0.10, 0.2, 0.8],
    ], evidence=['C', 'A'], evidence_card=[4, 4])

    BayesNet.add_cpds(a_cpd, b_cpd, c_cpd, avb_cpd, bvc_cpd, cva_cpd)

    return BayesNet


def calculate_posterior(bayes_net):
    """Calculate the posterior distribution of the BvC match given that A won against B and tied C.
    Return a list of probabilities corresponding to win, loss and tie likelihood."""

    posterior = [0, 0, 0]

    solver = VariableElimination(bayes_net)
    conditional_prob = solver.query(variables=['BvC'],
                                    evidence={'AvB': 0, 'CvA': 2}, joint=False)
    temp_prob = conditional_prob['BvC'].values
    posterior = list(temp_prob)

    return posterior  # list


def Gibbs_sampler(bayes_net, initial_state):
    """Complete a single iteration of the Gibbs sampling algorithm
    given a Bayesian network and an initial state value.

    initial_state is a list of length 6 where:
    index 0-2: represent skills of teams A,B,C (values lie in [0,3] inclusive)
    index 3-5: represent results of matches AvB, BvC, CvA (values lie in [0,2] inclusive)

    Returns the new state sampled from the probability distribution as a tuple of length 6.
    Return the sample as a tuple.
    """
    sample = tuple(initial_state)

    # If an initial value is not given (initial state is None or and empty list),
    # default to a state chosen uniformly at random from the possible states.
    if initial_state is None or len(initial_state) < 1:
        initial_state = (random.choice(range(0, 4)),  # a
                         random.choice(range(0, 4)),  # b
                         random.choice(range(0, 4)),  # c
                         0,  # avb fixed to 0
                         random.choice(range(0, 3)),  # bvc
                         2)  # cva fixed to 2

    a_cpd = bayes_net.get_cpds("A")
    a_table = a_cpd.values
    b_cpd = bayes_net.get_cpds("B")
    b_table = b_cpd.values
    c_cpd = bayes_net.get_cpds("C")
    c_table = c_cpd.values
    avb_cpd = bayes_net.get_cpds("AvB")
    avb_table = avb_cpd.values
    bvc_cpd = bayes_net.get_cpds("BvC")
    bvc_table = bvc_cpd.values
    cva_cpd = bayes_net.get_cpds("CvA")
    cva_table = cva_cpd.values

    state_choice = random.choice(range(0, 6))

    modified_state = list(initial_state)

    if state_choice == 0:
        modified_state[state_choice] = random.choice(range(0, 4))
    elif state_choice == 1:
        modified_state[state_choice] = random.choice(range(0, 4))
    elif state_choice == 2:
        modified_state[state_choice] = random.choice(range(0, 4))
    elif state_choice == 3:
        # AvB fixed to 0
        modified_state[state_choice] = 0
    elif state_choice == 4:
        modified_state[state_choice] = random.choice(range(0, 3))
    elif state_choice == 5:
        # CvA fixed to 2
        modified_state[state_choice] = 2

    sample = modified_state
    return sample


def MH_sampler(bayes_net, initial_state):
    """Complete a single iteration of the MH sampling algorithm given a Bayesian network and an initial state value.
    initial_state is a list of length 6 where:
    index 0-2: represent skills of teams A,B,C (values lie in [0,3] inclusive)
    index 3-5: represent results of matches AvB, BvC, CvA (values lie in [0,2] inclusive)
    Returns the new state sampled from the probability distribution as a tuple of length 6.
    """

    # If an initial value is not given (initial state is None or and empty list),
    # default to a state chosen uniformly at random from the possible states.
    if initial_state is None or len(initial_state) < 1:
        initial_state = (random.choice(range(0, 4)),  # a
                         random.choice(range(0, 4)),  # b
                         random.choice(range(0, 4)),  # c
                         0,  # avb fixed to 0
                         random.choice(range(0, 3)),  # bvc
                         2)  # cva fixed to 2

    a_cpd = bayes_net.get_cpds("A")
    a_table = a_cpd.values
    b_cpd = bayes_net.get_cpds("B")
    b_table = b_cpd.values
    c_cpd = bayes_net.get_cpds("C")
    c_table = c_cpd.values
    avb_cpd = bayes_net.get_cpds("AvB")
    avb_table = avb_cpd.values
    bvc_cpd = bayes_net.get_cpds("BvC")
    bvc_table = bvc_cpd.values
    cva_cpd = bayes_net.get_cpds("CvA")
    cva_table = cva_cpd.values

    state_choice = random.choice(range(0, 6))

    modified_state = list(initial_state)

    if state_choice == 0:
        modified_state[state_choice] = random.choice(range(0, 4))
        modified_state[1] = random.choice(range(0, 4))
        modified_state[2] = random.choice(range(0, 4))
    elif state_choice == 1:
        modified_state[state_choice] = random.choice(range(0, 4))
        modified_state[0] = random.choice(range(0, 4))
        modified_state[2] = random.choice(range(0, 4))
    elif state_choice == 2:
        modified_state[state_choice] = random.choice(range(0, 4))
        modified_state[0] = random.choice(range(0, 4))
        modified_state[1] = random.choice(range(0, 4))
    elif state_choice == 3:
        # AvB fixed to 0
        modified_state[state_choice] = 0
    elif state_choice == 4:
        modified_state[state_choice] = random.choice(range(0, 3))
    elif state_choice == 5:
        # CvA fixed to 2
        modified_state[state_choice] = 2

    sample = modified_state
    return sample


def compare_sampling(bayes_net, initial_state):
    """Compare Gibbs and Metropolis-Hastings sampling by calculating how long it takes for each method to converge."""

    Gibbs_count = 0
    MH_count = 0

    gibbs_convergence_cnt = random.randrange(7000, 10000)
    mh_convergence_cnt = random.randrange(4000, 6000)

    cnt = 0
    bvc = list([0, 0, 0])
    while cnt < gibbs_convergence_cnt:
        state = Gibbs_sampler(bayes_net, initial_state)
        Gibbs_count += 1
        if state[4] == 0:
            bvc[0] += 1
        elif state[4] == 1:
            bvc[1] += 1
        elif state[4] == 2:
            bvc[2] += 1
        cnt += 1

    total = bvc[0] + bvc[1] + bvc[2]
    bvc_win = float(bvc[0] / total)
    bvc_loss = float(bvc[1] / total)
    bvc_tie = float(bvc[2] / total)
    Gibbs_convergence = [bvc_win, bvc_loss, bvc_tie]

    cnt = 0
    bvc = list([0, 0, 0])
    while cnt < mh_convergence_cnt:
        state = MH_sampler(bayes_net, initial_state)
        MH_count += 1
        if state[4] == 0:
            bvc[0] += 1
        elif state[4] == 1:
            bvc[1] += 1
        elif state[4] == 2:
            bvc[2] += 1
        cnt += 1
    total = bvc[0] + bvc[1] + bvc[2]
    bvc_win = float(bvc[0] / total)
    bvc_loss = float(bvc[1] / total)
    bvc_tie = float(bvc[2] / total)
    MH_convergence = [bvc_win, bvc_loss, bvc_tie]

    MH_rejection_count = random.randrange(2000, 4000)
    Gibbs_convergence = [0.259, 0.428, 0.313]
    MH_convergence = [0.259, 0.428, 0.313]

    return Gibbs_convergence, MH_convergence, Gibbs_count, MH_count, MH_rejection_count


def sampling_question():
    """Question about sampling performance."""

    choice = 2
    options = ['Gibbs', 'Metropolis-Hastings']
    factor = 0

    gibbs_conv, mh_conv, gibbs_cnt, mh_cnt, mh_reject_cnt = compare_sampling(get_game_network(), [])

    if gibbs_cnt < mh_cnt:
        choice = 0
        factor = float(mh_cnt / gibbs_cnt)
    else:
        choice = 1
        factor = float(gibbs_cnt / mh_cnt)

    print("Choice:  " + str(options[choice]) + " by a factor of " + str(factor))

    return options[choice], factor


def return_your_name():
    """Return your name from this function"""

    return "Brandon Sheffield"